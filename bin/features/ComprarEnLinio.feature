@TodoslosCasos
Feature: Comprar por la pagina de Linio el producto deseado
	validado que este en el carrito de compras

	@CasoExitoso
	Scenario: Comprar en la pagina de Linio el celular deseado
		Given ingreso a la pagina de Linio
		When realizo la busqueda del celular huawei mate 20 pro
		And selecciono el producto
		And adiciono el producto al carrito de compras
		Then se valida que en el carrito aparezca el producto seleccionado con su cantidad