package userinterfaces;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://www.linio.com.co/")
public class PaginaLinioUserInterface extends PageObject{
	public static final Target BUSCA_PRODUCTOS = Target.the("Para buscar productos").
			located(By.xpath("//*[@placeholder='Busca productos']"));
	public static final Target BUSCAR = Target.the("Click para buscar productos").
			located(By.xpath("//*[@class='icon icon-inverse']"));
	public static final Target PRODUCTO_SELECCIONADO = Target.the("Click en el producto deseado").
			located(By.xpath("//*[@alt='Celular Huawei P10 - Dorado 32GB (P10)']"));
	public static final Target A�ADIR_AL_CARRITO = Target.the("Click en a�adir al carrito").
			located(By.xpath("//*[@class='btn btn-lg hidden-sm-down btn-primary']"));
	public static final Target IR_AL_CARRITO = Target.the("Click en ir al carrito").
			located(By.xpath("//*[@class='btn btn-sm btn-go-to-cart']"));
	public static final Target TOTAL_COMPRA = Target.the("Campo de total de la compra").
			located(By.xpath("//*[@class='price-secondary pull-xs-right' and @ng-bind='cart.data.grandTotal|formatMoney']"));
	public static final Target LISTA_PRODUCTOS = Target.the("Click en la algun elemento de la lista").
			located(By.xpath("//*[@class='catalogue-product-section col-lg-10 col-md-9']"));
	public static final Target PRECIO = Target.the("Captura el precio").
			located(By.xpath("//*[@class='price price-main']"));		
}
