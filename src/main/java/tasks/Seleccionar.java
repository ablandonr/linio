package tasks;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import userinterfaces.PaginaLinioUserInterface;

public class Seleccionar implements Task{

	public int aleatorio= 0;
	@Override
	public <T extends Actor> void performAs(T aleja) {
		List<WebElement> listaProductos = PaginaLinioUserInterface.LISTA_PRODUCTOS.resolveFor(aleja).findElements(By.xpath("//*[@class='title-section']"));
		aleatorio=(int) (Math.random() * listaProductos.size()) + 1;
		for(int i=0; i<listaProductos.size(); i++ ){
        	if(i==aleatorio) {
        		listaProductos.get(i).click();
        		break;
        	}
        }
	}

	public static Seleccionar elProducto() {
		return Tasks.instrumented(Seleccionar.class);
	}
	

}
