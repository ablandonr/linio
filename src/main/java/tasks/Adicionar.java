package tasks;

import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import userinterfaces.PaginaLinioUserInterface;

public class Adicionar implements Task{

	public String precio = null;
	@Override
	public <T extends Actor> void performAs(T aleja) {
		precio=PaginaLinioUserInterface.PRECIO.resolveFor(aleja).getText();
		Serenity.setSessionVariable("precioProducto").to(precio);
		aleja.attemptsTo(Click.on(PaginaLinioUserInterface.A�ADIR_AL_CARRITO),
				Click.on(PaginaLinioUserInterface.IR_AL_CARRITO));
	}

	public static Adicionar alCarrito() {
		return Tasks.instrumented(Adicionar.class);
	}

}
