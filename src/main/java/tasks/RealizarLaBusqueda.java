package tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import userinterfaces.PaginaLinioUserInterface;

public class RealizarLaBusqueda implements Task{

	public String especificado;
	public RealizarLaBusqueda(String especificado) {
		this.especificado = especificado;
	}

	@Override
	public <T extends Actor> void performAs(T aleja) {
		aleja.attemptsTo(Enter.theValue(especificado).into(PaginaLinioUserInterface.BUSCA_PRODUCTOS),
				Click.on(PaginaLinioUserInterface.BUSCAR));
		
	}

	public static RealizarLaBusqueda delProducto(String especificado) {
		return Tasks.instrumented(RealizarLaBusqueda.class, especificado);
	}

}
