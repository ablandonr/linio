package questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;
import userinterfaces.PaginaLinioUserInterface;

public class EnElCarrito implements Question<String>{

	@Override
	public String answeredBy(Actor aleja) {
		return Text.of(PaginaLinioUserInterface.TOTAL_COMPRA).viewedBy(aleja).asString();
	}

	public static EnElCarrito deCompras() {
		return new EnElCarrito();
	}

}
