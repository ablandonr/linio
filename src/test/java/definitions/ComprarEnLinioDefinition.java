package definitions;

import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.Matchers.equalTo;

import org.openqa.selenium.WebDriver;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.thucydides.core.annotations.Managed;
import questions.EnElCarrito;
import tasks.Adicionar;
import tasks.RealizarLaBusqueda;
import tasks.Seleccionar;
import userinterfaces.PaginaLinioUserInterface;

public class ComprarEnLinioDefinition {
	
	@Managed(driver="chrome")
	private WebDriver navegadorChrome;
	
	@Before
	public void configuracionInicial() {
		OnStage.setTheStage(new OnlineCast());
		theActorCalled("Aleja").can(BrowseTheWeb.with(navegadorChrome));
	}
	
	@Given("^ingreso a la pagina de Linio$")
	public void ingresoALaPaginaDeLinio() {
		theActorInTheSpotlight().wasAbleTo(Open.browserOn(new PaginaLinioUserInterface()));
	}

	@When("^realizo la busqueda del (.*)$")
	public void realizoLaBusquedaDel(String especificado) {
		theActorInTheSpotlight().attemptsTo(RealizarLaBusqueda.delProducto(especificado));
	}

	@When("^selecciono el producto$")
	public void seleccionoElProducto() {
		theActorInTheSpotlight().attemptsTo(Seleccionar.elProducto());
	}

	@When("^adiciono el producto al carrito de compras$")
	public void adicionoElProductoAlCarritoDeCompras() {
		theActorInTheSpotlight().attemptsTo(Adicionar.alCarrito());
	}

	@Then("^se valida que en el carrito aparezca el producto seleccionado con su cantidad$")
	public void seValidaQueEnElCarritoAparezcaElProductoSeleccionadoConSuCantidad() {
		theActorInTheSpotlight().should(GivenWhenThen.seeThat(EnElCarrito.deCompras(), equalTo(Serenity.sessionVariableCalled("precioProducto"))));
	}

}
